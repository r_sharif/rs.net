﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RsNet.Commons.Network;

namespace RsNet.Game
{
    /// <summary>
    /// 
    /// </summary>
    public class GameConnectionHandler : SimpleConnectionHandler
    {
        /// <summary>
        /// Called when [connection].
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <exception cref="NotImplementedException"></exception>
        public override void OnAddConnection(Connection connection)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Called when [remove connection].
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <exception cref="NotImplementedException"></exception>
        public override void OnRemoveConnection(Connection connection)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the maximum connections.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public override int GetMaxConnections()
        {
            throw new NotImplementedException();
        }
    }
}
