﻿using System;

namespace RsNet.Commons.Events
{
    /// <summary>
    /// Holds all the event listener functions.
    /// </summary>
    internal class EventHandlerChain
    {
        /// <summary>
        /// Holds the listener functions.
        /// </summary>
        private event EventHandler _handler;

        /// <summary>
        /// Constructs a new event handler chain instance.
        /// </summary>
        /// <param name="handler">Holds the listener functions.</param>
        internal EventHandlerChain(EventHandler handler)
        {
            _handler = handler;
        }

        /// <summary>
        /// Holds the listener functions.
        /// </summary>
        internal event EventHandler Handler
        {
            add
            {
                _handler += value;
            }
            remove
            {
                _handler -= value;
            }
        }

        /// <summary>
        /// Starts all the listener functions.
        /// </summary>
        /// <param name="sender">The object that called the call method in event dispatcher.</param>
        /// <param name="e">The event arguments.</param>
        internal void Start(object sender, EventArgs e)
        {
            _handler(sender, e);
        }
    }
}