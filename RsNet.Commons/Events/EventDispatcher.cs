﻿using System;
using System.Collections.Generic;

namespace RsNet.Commons.Events
{
    /// <summary>
    /// Stores IEventListener implementations for Events. This class also executes IEventListener implementations when an Event gets called.
    /// </summary>
    public class EventDispatcher
    {
        /// <summary>
        /// Stores event handlers for many different event types in a chain.
        /// </summary>
        private IDictionary<Type, EventHandlerChain> _handlers = new Dictionary<Type, EventHandlerChain>();

        /// <summary>
        /// Adds an event handler to the dictionary of handlers.
        /// </summary>
        /// <param name="type">The type of event the handler is for.</param>
        /// <param name="listener">The IEventListener implementation.</param>
        public void Add(Type type, IEventListener listener)
        {
            EventHandlerChain chain = null;
            if (_handlers.ContainsKey(type))
            {
                chain = _handlers[type];
                chain.Handler += listener.Listen;
            }
            else
            {
                chain = new EventHandlerChain(listener.Listen);
                _handlers.Add(type, chain);
            }
        }

        /// <summary>
        /// Removes all the event handlers for events of the defined type from the event handler dictionary.
        /// </summary>
        /// <param name="type">The type of event we are removing the handlers for.</param>
        public void Remove(Type type)
        {
            _handlers.Remove(type);
        }

        /// <summary>
        /// Executes all the handlers for the type of the defined event.
        /// </summary>
        /// <param name="e">The event instance.</param>
        public void Call(object sender, EventArgs e)
        {
            Type eventType = e.GetType();
            EventHandlerChain chain = _handlers[eventType];
            chain.Start(sender, e);
        }
    }
}