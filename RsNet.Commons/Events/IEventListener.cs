﻿using System;

namespace RsNet.Commons.Events
{
    /// <summary>
    /// Represents an event handler.
    /// </summary>
    public interface IEventListener
    {
        /// <summary>
        /// This method is called when an event is pushed to the event dispatcher.
        /// </summary>
        /// <param name="sender">The object that called the event call method in the event dispatcher.</param>
        /// <param name="e">The event that has been pushed to the event dispatcher.</param>
        void Listen(object sender, EventArgs e);
    }
}