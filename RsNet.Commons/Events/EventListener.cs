﻿
using System;

namespace RsNet.Commons.Events
{
    /// <summary>
    /// A simple implementation of IEventListener to handle generic types.
    /// </summary>
    /// <typeparam name="E"></typeparam>
    public abstract class EventListener<E> : IEventListener where E : EventArgs
    {
        /// <summary>
        /// This method is called when an event is pushed to the event dispatcher.
        /// </summary>
        /// <param name="sender">The object that called the event call method in the event dispatcher.</param>
        /// <param name="e">The event that has been pushed to the event dispatcher.</param>
        public void Listen(object sender, EventArgs e)
        {
            Preconditions.CheckArgument(e is E);
            E genericE = (E) e;
            Listen(sender, genericE);
        }

        /// <summary>
        /// Listens the specified sender.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The e.</param>
        public abstract void Listen(object sender, E e);
    }
}
