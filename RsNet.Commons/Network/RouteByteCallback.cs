﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RsNet.Commons.Network
{
    /// <summary>
    /// Routes the received data.
    /// </summary>
    /// <param name="data">The data to be routed.</param>
    public delegate void RouteByteCallback(byte[] data);
}