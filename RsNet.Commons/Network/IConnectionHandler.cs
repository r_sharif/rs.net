﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RsNet.Commons.Network
{
    /// <summary>
    /// Represents a connection handler.
    /// </summary>
    public interface IConnectionHandler
    {
        /// <summary>
        /// Called when [stop].
        /// </summary>
        void OnStop();

        /// <summary>
        /// Called when [new connection].
        /// </summary>
        /// <param name="connection">The connection.</param>
        void OnNewConnection(Connection connection);

        /// <summary>
        /// Called when [disconnection].
        /// </summary>
        /// <param name="connection">The connection.</param>
        void OnDisconnection(Connection connection);
    }
}
