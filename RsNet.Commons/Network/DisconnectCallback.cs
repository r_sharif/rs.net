﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RsNet.Commons.Network
{
    /// <summary>
    /// Disconnects the connection.
    /// </summary>
    public delegate void DisconnectCallback();
}
