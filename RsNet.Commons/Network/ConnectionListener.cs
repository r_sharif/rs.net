﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace RsNet.Commons.Network
{
    /// <summary>
    /// Listens for connections from the client.
    /// </summary>
    public class ConnectionListener: IService
    {
        /// <summary>
        /// The connection handler
        /// </summary>
        private IConnectionHandler _handler;

        /// <summary>
        /// The transmissions listener
        /// </summary>
        private TcpListener _transmissionsListener;

        /// <summary>
        /// The connection received
        /// </summary>
        private AsyncCallback _connectionReceived;

        /// <summary>
        /// Initializes a new instance of the <see cref="ConnectionListener"/> class.
        /// </summary>
        /// <param name="port">The port.</param>
        /// <param name="handler">The handler.</param>
        public ConnectionListener(int port, IConnectionHandler handler)
        {
            _transmissionsListener = new TcpListener(new IPEndPoint(IPAddress.Any, port));
            _connectionReceived = new AsyncCallback(OnConnectionReceived);
            _handler = handler;
        }

        /// <summary>
        /// Starts this instance.
        /// </summary>
        public void Start()
        {
            _transmissionsListener.Start(5);
            StandBy();
        }

        /// <summary>
        /// Stops this instance.
        /// </summary>
        public void Stop()
        {
            _transmissionsListener.Stop();
            _handler.OnStop();
        }

        /// <summary>
        /// Waits this instance.
        /// </summary>
        public void StandBy()
        {
            _transmissionsListener.BeginAcceptSocket(_connectionReceived, null);
        }

        /// <summary>
        /// Called when [connection received].
        /// </summary>
        /// <param name="result">The result.</param>
        public void OnConnectionReceived(IAsyncResult result)
        {
            Socket socket = _transmissionsListener.EndAcceptSocket(result);
            StandBy();
            Connection connection = Preconditions.CheckNotNull(new Connection(socket));
            _handler.OnNewConnection(connection);
        }
    }
}
