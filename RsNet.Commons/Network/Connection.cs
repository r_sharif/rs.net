﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;

namespace RsNet.Commons.Network
{
    /// <summary>
    /// Represents a connection in a network.
    /// </summary>
    public class Connection
    {
        /// <summary>
        /// The byte buffer
        /// </summary>
        private byte[] _buffer;

        /// <summary>
        /// The bytes received
        /// </summary>
        private AsyncCallback _bytesReceived;
        
        /// <summary>
        /// The _byte router
        /// </summary>
        private RouteByteCallback _byteRouter = new RouteByteCallback((bytes) => { });

        /// <summary>
        /// The _disconnector
        /// </summary>
        private DisconnectCallback _disconnector = new DisconnectCallback(() => { });

                /// <summary>
        /// Gets the socket.
        /// </summary>
        /// <value>
        /// The socket.
        /// </value>
        public Socket Socket { get; private set; }

        /// <summary>
        /// Gets the ip address.
        /// </summary>
        /// <value>
        /// The ip address.
        /// </value>
        public string IPAddress
        {
            get
            {
                return Socket == null ? "" : Socket.RemoteEndPoint.ToString().Split(':')[0];
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Connection"/> class.
        /// </summary>
        /// <param name="socket">The socket.</param>
        public Connection(Socket socket)
        {
            Socket = socket;
        }

        /// <summary>
        /// Listens the specified router.
        /// </summary>
        /// <param name="router">The router.</param>
        /// <param name="disconnector">The disconnector.</param>
        public void Listen(RouteByteCallback router, DisconnectCallback disconnector)
        {
            _buffer = new byte[512];
            _bytesReceived = new AsyncCallback(OnBytesReceived);
            _disconnector = disconnector;
            _byteRouter = router;
            Wait();
        }

        /// <summary>
        /// Called when [bytes received].
        /// </summary>
        /// <param name="result">The result.</param>
        private void OnBytesReceived(IAsyncResult result)
        {
            int received = Socket.EndReceive(result);
            Preconditions.CheckArgument(received != 0);
            byte[] bytes = new byte[received];
            Buffer.BlockCopy(_buffer, 0, bytes, 0, received);
            _byteRouter(bytes);

        }

        /// <summary>
        /// Waits this instance.
        /// </summary>
        private void Wait()
        {
            Socket.BeginReceive(_buffer, 0, 512, SocketFlags.None, _bytesReceived, null);
        }

        public void OnDisconnect()
        {
            _disconnector();
        }
    }
}
