﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RsNet.Commons.Network
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class SimpleConnectionHandler : IConnectionHandler
    {
        /// <summary>
        /// Gets the connected clients.
        /// </summary>
        /// <value>
        /// The connected clients.
        /// </value>
        public List<Connection> ConnectedClients { get; private set; }

        /// <summary>
        /// Gets the maximum connections.
        /// </summary>
        /// <returns></returns>
        public abstract int GetMaxConnections();

        /// <summary>
        /// Initializes a new instance of the <see cref="SimpleConnectionHandler"/> class.
        /// </summary>
        public SimpleConnectionHandler()
        {
            ConnectedClients = new List<Connection>(GetMaxConnections());
        }

        /// <summary>
        /// Stops this instance.
        /// </summary>
        public void OnStop()
        {
            lock (ConnectedClients)
            {
                ConnectedClients.ForEach(client => client.OnDisconnect());
                ConnectedClients.Clear();
            }
        }

        /// <summary>
        /// Called when [new connection].
        /// </summary>
        /// <param name="connection">The connection.</param>
        public void OnNewConnection(Connection connection)
        {
            lock (ConnectedClients)
            {
                ConnectedClients.Add(connection);
            }
            OnAddConnection(connection);
        }

        /// <summary>
        /// Called when [connection].
        /// </summary>
        /// <param name="connection">The connection.</param>
        public abstract void OnAddConnection(Connection connection);

        /// <summary>
        /// Called when [disconnection].
        /// </summary>
        /// <param name="connection">The connection.</param>
        public void OnDisconnection(Connection connection)
        {
            lock (ConnectedClients)
            {
                Preconditions.CheckArgument(connection != null && ConnectedClients.Contains(connection));
                connection.Socket.Close();
                ConnectedClients.Remove(connection);
            }
            OnRemoveConnection(connection);
        }

        /// <summary>
        /// Called when [remove connection].
        /// </summary>
        /// <param name="connection">The connection.</param>
        public abstract void OnRemoveConnection(Connection connection);
    }
}