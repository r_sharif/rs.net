﻿using System.Xml;
using RsNet.Commons.Events;

namespace RsNet.Commons.Xml
{
    /// <summary>
    /// An event listener used for writing xml files.
    /// </summary>
    public class OutputXmlEventListener : EventListener<OutputXmlEventArgs>
    {
        /// <summary>
        /// This method is called when an event is pushed to the event dispatcher.
        /// </summary>
        /// <param name="sender">The object that called the event call method in the event dispatcher.</param>
        /// <param name="e">The event that has been pushed to the event dispatcher.</param>
        public override void Listen(object sender, OutputXmlEventArgs e)
        {
            using (XmlWriter writer = XmlWriter.Create(e.Name + ".xml"))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement(e.Name);
                e.OnOutput(sender, writer);
                writer.WriteEndElement();
                writer.WriteEndDocument();
            }
        }
    }
}
