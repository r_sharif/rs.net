﻿using System.Xml;

namespace RsNet.Commons.Xml
{
    /// <summary>
    /// Represents the arguments of an input xml event.
    /// </summary>
    public abstract class OutputXmlEventArgs : XmlEventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OutputXmlEventArgs"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        public OutputXmlEventArgs(string name)
            : base(name)
        {
        }

        /// <summary>
        /// Called when [output].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="writer">The writer.</param>
        public abstract void OnOutput(object sender, XmlWriter writer);
    }
}