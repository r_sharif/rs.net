﻿using System;
using System.Xml;
using RsNet.Commons.Events;

namespace RsNet.Commons.Xml
{
    /// <summary>
    /// An event listener used for reading xml files.
    /// </summary>
    public class InputXmlEventListener : EventListener<InputXmlEventArgs>
    {
        /// <summary>
        /// This method is called when an event is pushed to the event dispatcher.
        /// </summary>
        /// <param name="sender">The object that called the event call method in the event dispatcher.</param>
        /// <param name="e">The event that has been pushed to the event dispatcher.</param>
        /// <exception cref="Exception">Wrong event arguments</exception>
        public override void Listen(object sender, InputXmlEventArgs e)
        {
            string element = "";
            using (XmlReader reader = XmlReader.Create(e.Name + ".xml"))
            {
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.Element)
                    {
                        element = reader.Name;
                    }
                    else if (reader.NodeType == XmlNodeType.Text)
                    {
                        e.OnInput(sender, element, reader);
                    }
                }
            }
        }
    }
}
