﻿using System.Xml;

namespace RsNet.Commons.Xml
{
    /// <summary>
    /// Represents the arguments of an input xml event.
    /// </summary>
    public abstract class InputXmlEventArgs : XmlEventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InputXmlEventArgs"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        public InputXmlEventArgs(string name)
            : base(name)
        {
        }

        /// <summary>
        /// Called when [input].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="element">The element.</param>
        /// <param name="reader">The reader.</param>
        public abstract void OnInput(object sender, string element, XmlReader reader);
    }
}
