﻿using System;

namespace RsNet.Commons.Xml
{
    /// <summary>
    /// Represents the arguments of an Xml input or output event.
    /// </summary>
    public abstract class XmlEventArgs : EventArgs
    {

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        internal String Name { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="XmlEventArgs"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        internal XmlEventArgs(string name)
        {
            Name = name;
        }
    }
}
