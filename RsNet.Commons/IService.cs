﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RsNet.Commons
{
    /// <summary>
    /// Represents a service.
    /// </summary>
    public interface IService
    {
        /// <summary>
        /// Starts this instance.
        /// </summary>
        void Start();

        /// <summary>
        /// Stops this instance.
        /// </summary>
        void Stop();

        /// <summary>
        /// Stands the by.
        /// </summary>
        void StandBy();
    }
}
