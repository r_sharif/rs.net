﻿using System;

namespace RsNet.Commons
{
    /// <summary>
    /// A utility class used to check method preconditions.
    /// </summary>
    public class Preconditions
    {
        /// <summary>
        /// Prevents a default instance of the <see cref="Preconditions"/> class from being created.
        /// </summary>
        /// <exception cref="Exception">This class cannot be instantiated!</exception>
        private Preconditions()
        {
            throw new Exception("This class cannot be instantiated!");
        }

        /// <summary>
        /// Checks the not null.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj">The object.</param>
        /// <returns>The object</returns>
        /// <exception cref="NullReferenceException"></exception>
        public static T CheckNotNull<T>(T obj)
        {
            if (obj == null)
            {
                throw new NullReferenceException();
            }
            return obj;
        }

        /// <summary>
        /// Checks the argument.
        /// </summary>
        /// <param name="condition">if set to <c>true</c> [condition].</param>
        /// <exception cref="ArgumentException"></exception>
        public static void CheckArgument(bool condition)
        {
            if (!condition)
            {
                throw new ArgumentException();
            }
        }
    }
}
