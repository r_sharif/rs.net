﻿using System;

namespace RsNet.Commons.Test.Events
{
    internal class TestEventArgs : EventArgs
    {
        /// <summary>
        /// The message of the event
        /// </summary>
        internal String Message { get; private set; }

        /// <summary>
        /// Creates a new test event instance
        /// </summary>
        /// <param name="message">The message</param>
        internal TestEventArgs(string message)
        {
            Message = message;
        }
    }
}
