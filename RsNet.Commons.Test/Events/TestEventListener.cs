﻿using RsNet.Commons.Events;

namespace RsNet.Commons.Test.Events
{
    internal class TestEventListener : EventListener<TestEventArgs>
    {
        /// <summary>
        /// The value from the handled event
        /// </summary>
        internal string EventValue { get; private set; }

        /// <summary>
        /// Handles the event pushed in the parameter.
        /// </summary>
        /// <param name="e">The event</param>
        public override void Listen(object sender, TestEventArgs e)
        {
            EventValue = e.Message;
        }
    }
}
