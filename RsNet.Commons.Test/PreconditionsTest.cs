﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace RsNet.Commons.Test
{
    /// <summary>
    ///This is a test class for PreconditionsTest and is intended
    ///to contain all PreconditionsTest Unit Tests
    ///</summary>
    [TestClass()]
    public class PreconditionsTest
    {

        /// <summary>
        /// The test context instance
        /// </summary>
        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        /// <summary>
        /// Checks the argument test_ fail.
        /// </summary>
        [TestMethod()]
        [ExpectedException(typeof(ArgumentException))]
        public void CheckArgumentTest_Fail()
        {
            bool condition = 1 == 2; // TODO: Initialize to an appropriate value
            Preconditions.CheckArgument(condition);
        }

        /// <summary>
        /// Checks the argument test_ pass.
        /// </summary>
        [TestMethod()]
        public void CheckArgumentTest_Pass()
        {
            bool completed = false;
            bool condition = 1 == 1; // TODO: Initialize to an appropriate value
            Preconditions.CheckArgument(condition);
            completed = true;
            Assert.IsTrue(completed);
        }

        /// <summary>
        /// Checks the not null test_ pass.
        /// </summary>
        [TestMethod()]
        public void CheckNotNullTest_Pass()
        {
            bool completed = false;
            object obj = Preconditions.CheckNotNull(new Object());
            completed = true;
            Assert.IsTrue(completed);
        }

        /// <summary>
        /// Checks the not null test_ fail.
        /// </summary>
        [TestMethod()]
        [ExpectedException(typeof(NullReferenceException))]
        public void CheckNotNullTest_Fail()
        {
            object obj = null; // TODO: Initialize to an appropriate value
            Preconditions.CheckNotNull(obj);
        }
    }
}
