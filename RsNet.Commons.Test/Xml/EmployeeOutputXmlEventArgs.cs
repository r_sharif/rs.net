﻿using RsNet.Commons.Xml;

namespace RsNet.Commons.Test.Xml
{
    internal class EmployeeOutputXmlEventArgs : OutputXmlEventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EmployeeOutputXmlEventArgs"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        public EmployeeOutputXmlEventArgs(string name)
            : base(name)
        {
        }

        /// <summary>
        /// Called when [output].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="writer">The writer.</param>
        public override void OnOutput(object sender, System.Xml.XmlWriter writer)
        {
            Preconditions.CheckArgument(sender is Employee);
            Employee employee = (Employee) sender;
            writer.WriteElementString("Name", employee.Name);
            writer.WriteElementString("Hours", employee.Hours.ToString());
        }
    }
}
