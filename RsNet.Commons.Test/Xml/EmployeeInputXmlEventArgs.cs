﻿using System;
using RsNet.Commons.Xml;

namespace RsNet.Commons.Test.Xml
{
    /// <summary>
    /// 
    /// </summary>
    internal class EmployeeInputXmlEventArgs : InputXmlEventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EmployeeInputXmlEventArgs"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        public EmployeeInputXmlEventArgs(string name)
            : base(name)
        {
        }

        /// <summary>
        /// Called when [input].
        /// </summary>
        /// <param name="sender">The object sending the arguments</param>
        /// <param name="reader">The reader.</param>
        public override void OnInput(object sender, string element, System.Xml.XmlReader reader)
        {
            Preconditions.CheckArgument(sender is Employee);
            Employee employee = (Employee) sender;
            switch (element)
            {
                case "Name":
                    employee.Name = reader.Value;
                    break;
                case "Hours":
                    employee.Hours = Int32.Parse(reader.Value);
                    break;
            }
        }
    }
}
