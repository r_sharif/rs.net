﻿using System;

namespace RsNet.Commons.Test.Xml
{
    /// <summary>
    /// Represents an employee
    /// </summary>
    internal class Employee
    {
        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        internal String Name { get; set; }

        /// <summary>
        /// Gets the hours.
        /// </summary>
        /// <value>
        /// The hours.
        /// </value>
        internal int Hours { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Employee"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="hours">The hours.</param>
        internal Employee(string name, int hours)
        {
            Name = name;
            Hours = hours;
        }
    }
}
