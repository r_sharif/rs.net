﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RsNet.Commons.Events;
using RsNet.Commons.Test.Events;

namespace RsNet.Commons.Test
{
    /// <summary>
    ///This is a test class for NewEventDispatcherTest and is intended
    ///to contain all NewEventDispatcherTest Unit Tests
    ///</summary>
    [TestClass()]
    public class EventDispatcherTest
    {
        /// <summary>
        /// The test context instance
        /// </summary>
        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for NewEventDispatcher Constructor
        ///</summary>
        [TestMethod()]
        public void NewEventDispatcherConstructorTest()
        {
            EventDispatcher target = new EventDispatcher();
            Assert.IsNotNull(target);
        }

        /// <summary>
        ///A test for Add
        ///</summary>
        [TestMethod()]
        public void AddTest()
        {
            EventDispatcher target = new EventDispatcher(); // TODO: Initialize to an appropriate value
            TestEventArgs e = new TestEventArgs("hello world");
            Type type = e.GetType(); // TODO: Initialize to an appropriate value
            TestEventListener listener = new TestEventListener(); // TODO: Initialize to an appropriate value
            target.Add(type, listener);
        }

        /// <summary>
        ///A test for Call
        ///</summary>
        [TestMethod()]
        public void CallTest()
        {
            EventDispatcher target = new EventDispatcher(); // TODO: Initialize to an appropriate value
            TestEventArgs e = new TestEventArgs("hello world");
            Type type = e.GetType(); // TODO: Initialize to an appropriate value
            TestEventListener listener = new TestEventListener(); // TODO: Initialize to an appropriate value
            target.Add(type, listener);
            target.Call(this, e);
            string name = e.Message;
            Assert.AreEqual("hello world", name);
        }

        /// <summary>
        ///A test for Remove
        ///</summary>
        [TestMethod()]
        public void RemoveTest()
        {
            EventDispatcher target = new EventDispatcher(); // TODO: Initialize to an appropriate value
            TestEventArgs e = new TestEventArgs("hello world");
            Type type = e.GetType(); // TODO: Initialize to an appropriate value
            TestEventListener listener = new TestEventListener(); // TODO: Initialize to an appropriate value
            target.Add(type, listener);
            target.Remove(type);
        }
    }
}
